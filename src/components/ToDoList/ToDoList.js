export default {
    name: 'ToDoList',
    props: {},
    data () {
        return {
            excludeType: "",
            newItem : {
                task: "",
                status: "not-selected"
            },
            items: [
                { task : "Acheter du pain", status: "not-selected" },
                { task : "Vendre la voiture", status: "not-selected" }
            ]
        }
    },
    methods : {
        addTask: function () 
        {
            this.items.push({task: this.newItem.task, status: "not-selected"});
            this.newItem.task = "";
        },
        removeTask: function(item) 
        {
            let i = this.items.indexOf(item);
            this.items.splice(i, 1);
        },
        checkAllTasks: function () 
        {
            let status =  this.newItem.status === "selected" ? "not-selected" : "selected";
            for (let i=0; i < this.items.length; i++) 
                this.items[i].status = status;
        },
        filterTasks: function (excludeType) { this.excludeType = excludeType; }
    },
    computed: {
        totalTasks : function () {
            const tasksLeft =  this.items.filter(function(item) {
                return item.status == "not-selected";
            });
            return tasksLeft.length;
        },
        filteredTasks: function() {
            if (this.excludeType != "") 
                return this.items.filter(item => item.status != this.excludeType );
            else
                return this.items;
        }
    }
}